# Edited Tech Test

Thanks for taking the time to read this. This is built in Node with a mongodb database.


## How to Run

1. Download mongodb using npm, this will need to be installed if you don't have it (https://nodejs.org/en/download/)
2. Set up the database, open a terminal (or powershell, I assume these commands will work but I haven't tried on windows) and change into this project
3. Run 'mongoimport --db edited --collection items --file search_dataset.json --jsonArray'
4. If this is successful you will not have all the items in a database called edited and inside a collection called items, if not, make sure you're in this directory, mongodb has installed correctly (check this by running mongod in the terminal)
5. In another terminal window run 'mongod', this will start the mongo db server
6. In another terminal window, in this project directory the file to run is called script.js. To run this type 'node script.js XYZ', XYZ here is the search term you would like to query the database for. EG if you wanted to seaerch for yellow toywatch, type 'node script.js yellow toywatch' (the search string can be in or out of quotation marks)
7. This should have returned a table with the 10 highest matched items. Or an error if there has been a problem with the database.


## My Approach

I chose node and mongodb just because with a 2hr time limit I was most familiar with these technologies so thought I'd be able to get the most submitted.


## Relevance Scoring

I probably spent a bit too much time thinking about this.
What I decided to go with in the end was a program that first tried to see if there was a brand in the string that had been searched. My thinking was that if someone had typed in a brand, they were only interested in items from that brand.
So firstly it will run a query looking in the database for any occasions where name OR brand contain any of the words searched.
My thinking was to go in 'heavy' here and then trim down later, as opposed to doing many db queries.
Now we have an array of items but most with quite a loose association. eg If you searched 'superdry mens black shoes' you will have all items that contain mens, black, superdry and shoes in their description and name. So this would return items you certainly were not interested in such as brands called 'black' or names such as 'black dress'.
The next stage is to trim this down. Line 46 will try to find a brand in the search string. This will only work for single word brands so not ideal. It will go through the results and return the word that appears most in items.brand. Again this is not ideal but generally works for this demonstration.
Once we have the brand, we score the items with the scoreItems function.
If a brand is found, iterate through the items and give them 25 points if the brands match. This would be to rate more highly items which match the brand you took the trouble of typing out. Then remove all the items that don't match the brand and remove that string from the search array (I don't want to give any more points to items that include the brand in the name)
The next part is quite basic but loos through the items again and gives each item 25 points for every occurrence of a word from the search string.

So, this should mean that if you search for 'asos yellow jeans', it should do the following;
1. Get all items from the database where name or brand contain ['asos', 'yellow', 'jeans'], case insensitive
2. Identify that asos is a brand
3. Remove all items who's brand is not asos
4. Give each item 25 points for every time a search word appears in the name, excluding the brand.
5. Sort these results and print out the top 10 results.

## Extras
Performance, I didn't get round to that much performance considerations other than slimming down the items array as the search went along so there was not too much unnecessary iterating over huge arrays.
Prefixes. I believe this could be addressed by adding some extra options to the regex options in the query. m?


## Things I’d like to improve;
1. Identifying brands is not ideal, it will not work for brands with multiple word names.
2. Sanitise the input string, because this is is running queries directly it could lead to problems.
3. Have separate collections with brands
4. Have a way of identifying when male and female search terms are used. This would mean you could use 'starts with' or 'ends with' terms with regex
5. Some brands could also be search words such as 'long'. Some kind of check for this would be more accurate
6. This readme. I apologise for the spelling.
7. At the moment it just adds 25 points . I would like to change this so it was more weighted and a set number of points based on results returned I think this would be the main thing that I would improve.
8. Add return info on number of items returned, also maybe try and add in some sort of function that, if for example no items are returned from the database it will re run a query with a variation.
