var MongoClient = require('mongodb').MongoClient
const async = require('async')
const _ = require('lodash')

let connection = null
let database = null
let queryArray = null

const RETURN_LIMIT = 10

let args = process.argv
if (args.length < 3) {
  console.log('Please run with a search string. eg. node script.js "fancy hats"')
} else {
  args = args.slice(2, args.length) // drop the first 2 argument variables
  if (args.length === 1) {
    args = args[0].split(' ') // if it was sent as a string
  }
  regex = args.join('|')
  run()
}

function run() {
  let items = null
  let brand = null
  let start = Date.now()
  let fns = []
  fns.push(done => {
    connect(done)
  })
  fns.push(done => {
    searchByString((err, resp) => {
      if (resp) {
        items = resp
      }
      done(err)
    })
  })
  async.series(fns, (err) => {
    if (connection) {
      connection.close()
    }
    if (err) {
      console.log('Error: ', err)
    } else {
      let brand = identifyBrand(items)
      items = scoreItems(items, brand).reverse()
      console.table(items.slice(0, RETURN_LIMIT))
      console.log('Brand Found: ', brand)
      console.log('Duration: ', Date.now() - start)
    }
  })
}

function identifyBrand(items) {
  let brandSearchMap = {}
  args.forEach((searchString) => {
    brandSearchMap[searchString] = 0
    items.forEach((item) => {
      if (item && item.brand && _.isString(item.brand) && item.brand.toLowerCase() === searchString.toLowerCase()) {
        brandSearchMap[searchString]++
      }
    })
  })
  let count = 0
  let brand = null
  Object.keys(brandSearchMap).forEach((string) => {
    if (brandSearchMap[string] > count) {
      count = brandSearchMap[string]
      brand = string
    }
  })
  return brand ? brand : false
}

function scoreItems(items, brand) {
  items.forEach((item) => {
    item.score = 0
  })
  if (brand) {
    items.forEach((item) => {
      if (item && item.brand && item.brand.toLowerCase() === brand) {
        item.score += 25 // change to something more dynamic
      }
    })
    _.remove(items, (item) =>{
      return item.brand.toLowerCase() !== brand // remove other brands to slim down items and speed things up
    })
    // If a brand is found, dont search the name for it
    _.remove(args, (arg) => {
      return brand === arg
    })
  }
  args.forEach((arg) => {
    items.forEach((item) => {
      if (item && item.name && _.isString(item.name)) {
        let nameArray = item.name.split(' ').map((itemWord) => {
          return itemWord.toLowerCase()
        })
        if (_.includes(nameArray, arg)) {
          item.score += 25
        }
      }
    })
  })
  return _.sortBy(items, (item) => {
    return item.score
  })
}

function connect(callback) {
  let options = {
    useUnifiedTopology: true
  }
  MongoClient.connect('mongodb://localhost:27017', options, (err, dbResp) => {
    if (dbResp) {
      connection = dbResp
      database = dbResp.db('edited')
    }
    callback(err)
  })
}

function searchByString(callback) {
  let query = {
    $or: [{
        brand: {
          '$regex': regex,
          '$options': 'i' // case insensitive
        }
      },
      {
        name: {
          '$regex': regex,
          '$options': 'i'
        }
      }
    ]
  }
  database.collection('items')
    .find(query)
    .project({
      name: 1,
      _id: 0,
      brand: 1,
      id: 1
    })
    .toArray((err, resp) => {
      callback(err, resp)
    })
}
